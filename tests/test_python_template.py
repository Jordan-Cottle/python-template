""" Simple test module for testing package __init__

TODO: rename this file to `test_<your package name>`
"""

from python_template import __version__


def test_version():
    """Check that the version number has the expected value."""
    assert __version__ == "0.1.0"
